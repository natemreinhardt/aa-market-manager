��    )      d  ;   �      �  	   �  
   �  	   �  	   �     �     �     �     �     �     �     �               $     0     ?     N     ]     b     k     w     �     �     �     �     �     �     �     �     �  	   �               *     1     6     :     A     R     b  �  k     �       	     	        #     3  	   A     K     Y  	   m     w     �     �  	   �     �     �     �     �  	   �     �               &     @     G     b     i     p  	   ~  	   �  	   �     �     �  	   �     �     �     �     �     �     �     #                   '                        )                    %                 &          !                             	          
   $      (                                   "                     Buy Order Buy Orders Cancelled Character Corporation Duration Expired Expires Fetch Regions Issued Jita Comparison % Last Updated Location Location ID Market Browser Market Manager Minimum Volume Name Order ID Order Range Order State Owned Character Orders Owned Corporation Orders Price Pull Market Orders Quantity Region Sell Orders Solar System Station Structure Structure ID Structure Type Filter System Type URL Volume Volume Remaining Wallet Division Webhooks Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:48+0000
Last-Translator: Joel Falknau <ozirascal@gmail.com>, 2023
Language-Team: Korean (Korea) (https://app.transifex.com/alliance-auth/teams/107430/ko_KR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ko_KR
Plural-Forms: nplurals=1; plural=0;
 구매 주문 구매 주문 취소됨 캐릭터 코퍼레이션 만료 기한 만료됨 만료 기한 지역 불러오기 생성일 지타 비교가 % 마지막 갱신 위치 위치 ID 거래소 브라우저 거래소 관리자 최소 수량 이름 주문 ID 주문 거리 주문 상태 캐릭터의 주문 코퍼레이션의 주문 가격 시장 주문 불러오기 수량 지역 판매 주문 항성계 정거장 구조물 구조물 ID 구조물 유형 필터 항성계 유형 URL 수량 남은 수량 부서 계좌 웹훅 - Webhook 