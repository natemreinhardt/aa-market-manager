# Generated by Django 4.2.10 on 2024-05-11 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0051_alter_privateconfig_failure_reason'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='managedsupplyconfig',
            options={'verbose_name': 'Managed Supply Config', 'verbose_name_plural': 'Managed Supply Configs'},
        ),
        migrations.AlterModelOptions(
            name='priceconfig',
            options={'verbose_name': 'Price Config', 'verbose_name_plural': 'Price Configs'},
        ),
        migrations.AlterModelOptions(
            name='privateconfig',
            options={'verbose_name': 'Private Market Configuration', 'verbose_name_plural': 'Private Market Configurations'},
        ),
        migrations.AlterModelOptions(
            name='supplyconfig',
            options={'verbose_name': 'Supply Config', 'verbose_name_plural': 'Supply Configs'},
        ),
        migrations.AlterModelOptions(
            name='typestatistics',
            options={'default_permissions': (), 'verbose_name': 'Item Type Statistics', 'verbose_name_plural': 'Item Types Statistics'},
        ),
        migrations.AlterField(
            model_name='order',
            name='range',
            field=models.CharField(choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('10', '10'), ('20', '20'), ('30', '30'), ('40', '40'), ('region', 'Region'), ('solarsystem', 'Solar System'), ('station', 'Station')], help_text='Valid order range, numbers are ranges in jumps', max_length=12, verbose_name='Order Range'),
        ),
    ]
